import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import(/* webpackChunkName: "home" */ '../views/Home.vue')
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/findnumber',
    name: 'FindNumber',
    component: () => import(/* webpackChunkName: "findnumber" */ '../views/FindNumber.vue')
  },
  {
    path: '/findvalue',
    name: 'FindValue',
    component: () => import(/* webpackChunkName: "findvalue" */ '../views/FindValue.vue')
  },
  {
    path: '/mapsdirection',
    name: 'MapsDirection',
    component: () => import(/* webpackChunkName: "mapsdirection" */ '../views/MapsDirection.vue')
  },
  {
    path: '/linebot',
    name: 'LineBot',
    component: () => import(/* webpackChunkName: "linebot" */ '../views/LineBot.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
